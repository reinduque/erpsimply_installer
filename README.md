# ERP-Simply Install Script

ERPSimply Install Script
by Rein Duque

Make sure you can access ERPSimply Repositories first. Message me for access

## Installation procedure

##### 1. Download the script:
```
sudo wget https://gitlab.com/reinduque/erpsimply_installer/raw/master/erpsimply_install.sh
```
##### 2. Make the script executable
```
sudo chmod +x erpsimply_install.sh
```
##### 3. Execute the script:
```
sudo ./erpsimply_install.sh
```
